title: microcontroleurs

# Utiliser une carte Arduino, ou microBit pour découvrir les microcontroleurs

- Simulateurs en ligne

[Tinkercad](https://www.tinkercad.com/){target="_blank"} Plateforme destinée à la conception 3D, à l'électronique et au codage.

[Cyberbotics Webots](https://cyberbotics.com/){target="_blank"} Simulation de robot et ressource [Eduscol](https://eduscol.education.fr/sti/si-ens-paris-saclay/ressources_pedagogiques/covapsy-mise-en-oeuvre-du-simulateur-webots#description){target="_blank"}

- - - - - -

- Quick-pi par France IOI

[Site](https://quick-pi.org/index.html){target="_blank"} Une application web, du matériel et du contenu pédagogique pour commencer à programmer ses objets connectés en quelques minutes.

- - - - - -

- Carte Micro:Bit

[Démo](https://peertube.lyceeconnecte.fr/w/jrsmbRnxZArKEHffoDsBgR){target="_blank"} Vidéo de démonstration pour l'utilisation d'une carte micro:bit avec le logiciel Thonny par Pierre Marquestaut.

[Documentation BBC](https://microbit-micropython.readthedocs.io/en/latest/tutorials/introduction.html){target="_blank"} 


[Site Ostralo](http://numerique.ostralo.net/microbit/partie0_accueil/0_accueil.htm){target="_blank"} les bases de l'utilisation de la carte Micro:bit de la BBC avec le langage Python.

- - - - - -

- Carte Arduino

[Blog d'Eskimon](https://eskimon.fr/){target="_blank"} découverte d'Arduino...

- - - - - -

- Raspberry Pi et Pico

[Site Raspberry Pi Pico MicroPython](https://www.gcworks.fr/tutoriel/pico/RaspberryPiPicoMicroPython1.html){target="_blank"} de Christophe GUENEAU

[Repository mailing-nsi-pico.md](https://gist.github.com/mathieunicolas/bbe1db2821de8ceab30e0d9df8cf705d){target="_blank"} Retour d'expérience et pistes pédagogiques concernant les Raspberry Pi et Pico W  de Mathieu Nicolas