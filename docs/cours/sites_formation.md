title: sites_formations

# Livres de cours 

- [Apprendre à programmer avec Python3](https://inforef.be/swi/download/apprendre_python3_5.pdf){target="_blank"}  de Gérard Swinnen.

- [Une introduction à Python 3](https://perso.limsi.fr/pointal/_media/python:cours:courspython3.pdf){target="_blank"}  de Bob Cordeau & Laurent Pointal.


# Sites de cours en ligne sur la programmation


- FUN France Université Numérique [site FUN](https://www.fun-mooc.fr/fr/){target="_blank"} Formations informatiques diverses et  [Vidéos MOOC NSI](https://tube-sciences-technologies.apps.education.fr/c/moocnsi/videos?s=1){target="_blank"}  mettant à disposition le contenu du MOOC “NSI : les fondamentaux”.


- [OpenClassRoom](https://openclassrooms.com/fr/){target="_blank"} Formations informatiques diverses



- [W3Schools](https://www.w3schools.com/){target="_blank"} Cours sur différents langages (développement Web, Python, Java, C, C++, C#, Ruby...) + exemples

- [Resources for developers MDN](https://developer.mozilla.org/fr/){target="_blank"} Aide pour le développement Web (HTML, CSS, Javascript) par Mozilla

- [Zest de Savoir](https://zestedesavoir.com/){target="_blank"} un site de partage de connaissances 

- [Apprendre le langage Haskell](http://www.lyah.haskell.fr/chapitres){target="_blank"}
