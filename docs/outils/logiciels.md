title: logiciels

# Logiciels

- VS Codium :

[site officiel](https://vscodium.com/){target="_blank"}

[Présentation Franck Chambon](https://fchambon.forge.apps.education.fr/classe/2-%C3%89diteurs/4-vscodium/){target="_blank"}

[Trucs et astuces](https://humanize.me/nerd/vscode-vscodium.html#ajouter-un-snippet-bloc-de-code-python-pour-markdown){target="_blank"} Astuces pour VSCodium
- - - - - -

- IDE Theia  :

[site officiel](https://eclipsesource.com/blogs/2024/06/27/introducing-the-theia-ide/){target="_blank"} a code development tool that stands out for its foundation on modern web technologies, enabling it to run seamlessly both on the desktop and in the browser

- - - - - -

- Vanilla-js  :

[site officiel](http://vanilla-js.com/){target="_blank"} a fast, lightweight, cross-platform framework
for building incredible, powerful JavaScript applications

- - - - - -

- Jupyter Notebook :

[site officiel](https://jupyter.org/){target="_blank"}

[Raccourcis clavier](https://cheatography.com/weidadeyue/cheat-sheets/jupyter-notebook/){target="_blank"}

[Colaboratory](https://colab.research.google.com/notebooks/welcome.ipynb){target="_blank"}

[JupyterHub](https://gitlab.com/jocelin.devalette/jupyterhub-pour-nsi){target="_blank"} un outil permettant d'ajouter une fonctionnalité multi-utilisateurs à Jupyter, par Jocelin Devalette.

[nbgrader](https://github.com/jupyter/nbgrader){target="_blank"}

- - - - - -

- Filius : simulation d'un réseau informatique
 
[site officiel](https://www.lernsoftware-filius.de/Herunterladen){target="_blank"}

[Guide Daniel Garmann](https://www.pearltrees.com/s/file/preview/205382473/Introduction%20Filius.pdf?pearlId=270715447){target="_blank"}

[Ressources Académie de Bordeaux](https://ent2d.ac-bordeaux.fr/disciplines/sti-college/2019/09/25/filius-un-logiciel-de-simulation-de-reseau-simple-et-accessible/){target="_blank"}

[Vidéo de David Roche](https://www.youtube.com/watch?v=K3GGmiLwB6U){target="_blank"}

- - - - - -

- PyScript : exécuter du code Python dans une page Web, à l'aide d'une seule chaîne JS

[Anaconda](https://engineering.anaconda.com/2022/04/welcome-pyscript.html){target="_blank"}

[Site Pyscript](https://pyscript.net/){target="_blank"}
 
[Exemples](https://pyscript.net/examples/){target="_blank"}

[Site RealPython](https://realpython.com/pyscript-python-in-browser/){target="_blank"}

[Exemple intégration Notebook](https://ericecmorlaix.github.io/test_MkDocs_PyScript/JupyterLike_REPL/){target="_blank"} par Eric Madex

[eskool essai pyscript](https://eskool.gitlab.io/tnsi/ressources/essai_pyscript/){target="_blank"} par Rodrigo Schwencke

- - - - - -

- StarBoard-notebook : an in-browser literate notebook that is extendable, sharable and hackable.

[Repository](https://github.com/gzuidhof/starboard-notebook){target="_blank"}



- - - - - -

- Création d'activitées interactives avec H5P:

[article LinuxFr.org](https://linuxfr.org/news/h5p-un-outil-libre-pour-creer-des-activites-interactives){target="_blank"} présentation de l'outil

[site officiel H5P](https://h5p.org/){target="_blank"} un outil libre pour créer des activités interactives 

- - - - - -

[Lumni](https://app.lumi.education/){target="_blank"} Créez du contenu interactif et engageant, découvrir le contenu d'autres créateurs, partagez votre propre contenu, rendre votre contenu disponible pour vos apprenants.


- - - - - 

- [OBS-studio](https://humanize.me/nerd/obs.html){target="_blank"} Tutoriel pour utiliser le logiciel OBS qui permet d'enregistrer son écran (donc de créer des « replays ») et également de streamer (donc de créer des flux vidéo en direct), par Jeremy Canavesio.

- - - - - 

- [Raylib](https://www.raylib.com/){target="_blank"} a programming library to enjoy videogames programming; no fancy interface, no visual helpers, no gui tools or editors.

